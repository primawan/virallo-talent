import { NgModule } from '@angular/core';
import { CommonCoreModule } from '../common/common.module';

import { OnboardRoutingModule } from './onboard-routing.module';
import { OnboardPageComponent } from './components/onboard-page/onboard-page.component';
import { OnboardSocialsComponent } from './components/onboard-socials/onboard-socials.component';
import { AddInstagramComponent } from './components/add-instagram/add-instagram.component';

@NgModule({
  imports: [
    CommonCoreModule,
    OnboardRoutingModule
  ],
  declarations: [
    OnboardPageComponent,
    OnboardSocialsComponent,
    AddInstagramComponent
  ]
})
export class OnboardModule { }
