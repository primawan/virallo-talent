import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnboardPageComponent } from './components/onboard-page/onboard-page.component';
import { OnboardSocialsComponent } from './components/onboard-socials/onboard-socials.component';
import { AddInstagramComponent } from './components/add-instagram/add-instagram.component';

const routes: Routes = [
  { path: '', component: OnboardPageComponent,
    children: [
      { path: '', component: OnboardSocialsComponent },
      { path: 'add-instagram', component: AddInstagramComponent }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardRoutingModule { }
