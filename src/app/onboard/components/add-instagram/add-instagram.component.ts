import { Component, OnInit } from '@angular/core';
import { INSTA_GIF_URL } from '../../../common/constants/ASSETS';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-instagram',
  templateUrl: './add-instagram.component.html',
  styleUrls: ['./add-instagram.component.scss']
})
export class AddInstagramComponent implements OnInit {
  instaGif = INSTA_GIF_URL;

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
