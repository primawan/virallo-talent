import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-onboard-socials',
  templateUrl: './onboard-socials.component.html',
  styleUrls: ['./onboard-socials.component.scss']
})
export class OnboardSocialsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToSocial(evt) {
    this.router.navigate(['/onboard/add-instagram']);
  }

}
