import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardSocialsComponent } from './onboard-socials.component';

describe('OnboardSocialsComponent', () => {
  let component: OnboardSocialsComponent;
  let fixture: ComponentFixture<OnboardSocialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardSocialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardSocialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
