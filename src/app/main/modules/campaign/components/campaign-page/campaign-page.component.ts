import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-campaign-page',
  templateUrl: './campaign-page.component.html',
  styleUrls: ['./campaign-page.component.scss']
})
export class CampaignPageComponent implements OnInit {
  tabs = [
    {label: 'Active', count: 6},
    {label: 'Past', count: 3},
    {label: 'Ignored', count: 1},
  ];
  campaigns = ['campaign', 'campaign', 'campaign', 'campaign', 'campaign', 'campaign'];
  index = 0;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToCampaign(url: string) {
    this.router.navigate(['/campaign', url]);
  }

}
