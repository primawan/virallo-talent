import { CampaignPageComponent } from './components/campaign-page/campaign-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CampaignPageComponent },
  { path: ':id', loadChildren: '../campaign-detail/campaign-detail.module#CampaignDetailModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignRoutingModule { }
