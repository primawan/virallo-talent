import { NgModule } from '@angular/core';

import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignPageComponent } from './components/campaign-page/campaign-page.component';
import { CampaignItemComponent } from './components/campaign-item/campaign-item.component';
import { CommonCoreModule } from '../../../common/common.module';

@NgModule({
  imports: [
    CommonCoreModule,
    CampaignRoutingModule
  ],
  declarations: [
    CampaignPageComponent,
    CampaignItemComponent
  ]
})
export class CampaignModule { }
