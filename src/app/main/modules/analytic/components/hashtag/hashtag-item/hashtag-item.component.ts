import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hashtag-item',
  templateUrl: './hashtag-item.component.html',
  styleUrls: ['./hashtag-item.component.scss']
})
export class HashtagItemComponent implements OnInit {
  hashtags = [
    { hashtag: 'kancut', likes: 1892 },
    { hashtag: 'hancur', likes: 1213 },
    { hashtag: 'semua', likes: 847 },
    { hashtag: 'harapan', likes: 534 },
    { hashtag: 'anda', likes: 128 },
  ];

  constructor() { }

  ngOnInit() {
  }

}
