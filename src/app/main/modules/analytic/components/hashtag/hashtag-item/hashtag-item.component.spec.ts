import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HashtagItemComponent } from './hashtag-item.component';

describe('HashtagItemComponent', () => {
  let component: HashtagItemComponent;
  let fixture: ComponentFixture<HashtagItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HashtagItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HashtagItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
