import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticListItemComponent } from './analytic-list-item.component';

describe('AnalyticListItemComponent', () => {
  let component: AnalyticListItemComponent;
  let fixture: ComponentFixture<AnalyticListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
