import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-analytic-list-item',
  templateUrl: './analytic-list-item.component.html',
  styleUrls: ['./analytic-list-item.component.scss']
})
export class AnalyticListItemComponent {
  @Input('icon') icon = 'favorite';
  @Input('number') number: number;
}
