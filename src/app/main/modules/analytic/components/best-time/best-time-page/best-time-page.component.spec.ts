import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestTimePageComponent } from './best-time-page.component';

describe('BestTimePageComponent', () => {
  let component: BestTimePageComponent;
  let fixture: ComponentFixture<BestTimePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestTimePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestTimePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
