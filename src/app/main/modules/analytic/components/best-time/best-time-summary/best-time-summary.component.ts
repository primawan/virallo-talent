import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-best-time-summary',
  templateUrl: './best-time-summary.component.html',
  styleUrls: ['./best-time-summary.component.scss']
})
export class BestTimeSummaryComponent implements OnInit {
  bestTimes = [1, 2, 3];

  constructor() { }

  ngOnInit() {
  }

}
