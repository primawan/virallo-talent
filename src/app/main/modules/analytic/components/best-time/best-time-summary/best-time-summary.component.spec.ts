import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestTimeSummaryComponent } from './best-time-summary.component';

describe('BestTimeSummaryComponent', () => {
  let component: BestTimeSummaryComponent;
  let fixture: ComponentFixture<BestTimeSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestTimeSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestTimeSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
