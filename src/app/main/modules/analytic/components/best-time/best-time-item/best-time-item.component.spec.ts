import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestTimeItemComponent } from './best-time-item.component';

describe('BestTimeItemComponent', () => {
  let component: BestTimeItemComponent;
  let fixture: ComponentFixture<BestTimeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestTimeItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestTimeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
