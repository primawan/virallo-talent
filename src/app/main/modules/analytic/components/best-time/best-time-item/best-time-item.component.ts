import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-best-time-item',
  templateUrl: './best-time-item.component.html',
  styleUrls: ['./best-time-item.component.scss']
})
export class BestTimeItemComponent implements OnInit {
  times = [1, 2, 3, 4];
  days = [1, 2, 3, 4, 5, 6, 7];

  constructor() { }

  ngOnInit() {
  }

}
