import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-summary-item',
  templateUrl: './summary-item.component.html',
  styleUrls: ['./summary-item.component.scss']
})
export class SummaryItemComponent implements OnInit {
  @Input('media') media = {} as any;
  @Input('top') top = true;

  constructor() { }

  ngOnInit() {
  }

}
