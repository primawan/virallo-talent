import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss']
})
export class SummaryPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  checkTop(): boolean {
    return window.innerWidth < 991;
  }

}
