import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticMenuComponent } from './analytic-menu.component';

describe('AnalyticMenuComponent', () => {
  let component: AnalyticMenuComponent;
  let fixture: ComponentFixture<AnalyticMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
