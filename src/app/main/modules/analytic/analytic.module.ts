import { NgModule } from '@angular/core';

import { AnalyticRoutingModule } from './analytic-routing.module';
import { AnalyticPageComponent } from './components/analytic-page/analytic-page.component';
import { AnalyticMenuComponent } from './components/analytic-menu/analytic-menu.component';
import { SummaryPageComponent } from './components/summary/summary-page/summary-page.component';
import { SummaryItemComponent } from './components/summary/summary-item/summary-item.component';
import { BestTimePageComponent } from './components/best-time/best-time-page/best-time-page.component';
import { HashtagPageComponent } from './components/hashtag/hashtag-page/hashtag-page.component';
import { CommonCoreModule } from '../../../common/common.module';
import { HashtagItemComponent } from './components/hashtag/hashtag-item/hashtag-item.component';
import { BestTimeSummaryComponent } from './components/best-time/best-time-summary/best-time-summary.component';
import { BestTimeItemComponent } from './components/best-time/best-time-item/best-time-item.component';
import { AnalyticListItemComponent } from './components/analytic-list-item/analytic-list-item.component';

@NgModule({
  imports: [
    CommonCoreModule,
    AnalyticRoutingModule
  ],
  declarations: [
    AnalyticPageComponent,
    AnalyticMenuComponent,
    SummaryPageComponent,
    SummaryItemComponent,
    BestTimePageComponent,
    HashtagPageComponent,
    HashtagItemComponent,
    BestTimeSummaryComponent,
    BestTimeItemComponent,
    AnalyticListItemComponent,
  ]
})
export class AnalyticModule { }
