import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticPageComponent } from './components/analytic-page/analytic-page.component';

const routes: Routes = [
  { path: '', component: AnalyticPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalyticRoutingModule { }
