import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountPageComponent } from './components/account-page/account-page.component';
import { ProfilePageComponent } from './components/profile/profile-page/profile-page.component';

const routes: Routes = [
  { path: '', component: AccountPageComponent,
    children: [
      { path: 'profile', component: ProfilePageComponent },
    ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
