import { CommonCoreModule } from './../../../common/common.module';
import { AccountPageComponent } from './components/account-page/account-page.component';
import { NgModule } from '@angular/core';

import { AccountRoutingModule } from './account-routing.module';
import { AccountMenuComponent } from './components/account-menu/account-menu.component';
import { ProfilePageComponent } from './components/profile/profile-page/profile-page.component';
import { ProfileAvatarComponent } from './components/profile/profile-avatar/profile-avatar.component';
import { ProfileBasicComponent } from './components/profile/profile-basic/profile-basic.component';
import { ProfileInterestComponent } from './components/profile/profile-interest/profile-interest.component';
import { ProfileCapabilityComponent } from './components/profile/profile-capability/profile-capability.component';

@NgModule({
  imports: [
    CommonCoreModule,
    AccountRoutingModule
  ],
  declarations: [
    AccountPageComponent,
    AccountMenuComponent,
    ProfilePageComponent,
    ProfileAvatarComponent,
    ProfileBasicComponent,
    ProfileInterestComponent,
    ProfileCapabilityComponent
  ]
})
export class AccountModule { }
