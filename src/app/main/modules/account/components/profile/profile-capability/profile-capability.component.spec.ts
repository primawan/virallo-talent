import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCapabilityComponent } from './profile-capability.component';

describe('ProfileCapabilityComponent', () => {
  let component: ProfileCapabilityComponent;
  let fixture: ComponentFixture<ProfileCapabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCapabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCapabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
