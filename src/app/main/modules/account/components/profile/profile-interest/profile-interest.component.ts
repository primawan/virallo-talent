import { Component, OnInit } from '@angular/core';

export interface DemoColor {
  name: string;
  color: string;
}

@Component({
  selector: 'app-profile-interest',
  templateUrl: './profile-interest.component.html',
  styleUrls: ['./profile-interest.component.scss']
})
export class ProfileInterestComponent implements OnInit {
  selectedColors: any[] = ['Primary', 'Warn'];
  availableColors: DemoColor[] = [
    { name: 'none', color: '' },
    { name: 'Primary', color: 'primary' },
    { name: 'Accent', color: 'accent' },
    { name: 'Warn', color: 'warn' }
  ];

  constructor() { }

  ngOnInit() {
  }

  removeColor(color: DemoColor) {
    let index = this.availableColors.indexOf(color);

    if (index >= 0) {
      this.availableColors.splice(index, 1);
    }

    index = this.selectedColors.indexOf(color.name);

    if (index >= 0) {
      this.selectedColors.splice(index, 1);
    }
  }

}
