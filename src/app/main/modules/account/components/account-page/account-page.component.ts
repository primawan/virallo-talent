import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ResponsiveHelper } from '../../../../../common/helpers/responsive.helper';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent {
  responsiveMenu = new ResponsiveHelper();

  constructor(private router: Router) { }

  checkMenu(): boolean {
    return this.responsiveMenu.checkMenu(this.router, '/account', '/account/profile');
  }

}
