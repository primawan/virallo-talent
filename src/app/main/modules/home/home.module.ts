import { NgModule } from '@angular/core';
import { CommonCoreModule } from '../../../common/common.module';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './components/home-page/home-page.component';
import { GuidelinesDialogComponent } from './components/guidelines/guidelines-dialog/guidelines-dialog.component';
import { GuidelinesContentComponent } from './components/guidelines/guidelines-content/guidelines-content.component';
import { HomeOfferComponent } from './components/home-offer/home-offer.component';
import { HomeTimelineComponent } from './components/home-timeline/home-timeline.component';
import { CampaignOfferComponent } from './components/item/campaign-offer/campaign-offer.component';
import { CampaignDoneComponent } from './components/item/campaign-done/campaign-done.component';

@NgModule({
  imports: [
    CommonCoreModule,
    HomeRoutingModule
  ],
  declarations: [
    HomePageComponent,
    GuidelinesDialogComponent,
    GuidelinesContentComponent,
    HomeOfferComponent,
    HomeTimelineComponent,
    CampaignOfferComponent,
    CampaignDoneComponent
  ],
  entryComponents: [
    GuidelinesDialogComponent
  ]
})
export class HomeModule { }
