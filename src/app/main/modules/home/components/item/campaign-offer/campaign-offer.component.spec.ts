import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignOfferComponent } from './campaign-offer.component';

describe('CampaignOfferComponent', () => {
  let component: CampaignOfferComponent;
  let fixture: ComponentFixture<CampaignOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
