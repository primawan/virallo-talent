import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDoneComponent } from './campaign-done.component';

describe('CampaignDoneComponent', () => {
  let component: CampaignDoneComponent;
  let fixture: ComponentFixture<CampaignDoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
