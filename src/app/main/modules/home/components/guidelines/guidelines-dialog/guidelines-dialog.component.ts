import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-guidelines-dialog',
  templateUrl: './guidelines-dialog.component.html',
  styleUrls: ['./guidelines-dialog.component.scss']
})
export class GuidelinesDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<GuidelinesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

}
