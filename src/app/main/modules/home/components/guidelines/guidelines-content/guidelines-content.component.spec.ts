import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesContentComponent } from './guidelines-content.component';

describe('GuidelinesContentComponent', () => {
  let component: GuidelinesContentComponent;
  let fixture: ComponentFixture<GuidelinesContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
