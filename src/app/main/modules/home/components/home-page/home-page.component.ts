import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GuidelinesDialogComponent } from '../guidelines/guidelines-dialog/guidelines-dialog.component';
import { DialogHelper } from '../../../../../common/helpers/dialog.helper';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  dialogRef;
  dialogHelper = new DialogHelper();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openGuide() {
    this.dialogRef = this.dialog.open(GuidelinesDialogComponent, this.dialogHelper.dialogOptions());
  }

}
