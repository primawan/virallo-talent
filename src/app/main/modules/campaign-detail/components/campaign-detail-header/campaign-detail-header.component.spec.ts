import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailHeaderComponent } from './campaign-detail-header.component';

describe('CampaignDetailHeaderComponent', () => {
  let component: CampaignDetailHeaderComponent;
  let fixture: ComponentFixture<CampaignDetailHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDetailHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
