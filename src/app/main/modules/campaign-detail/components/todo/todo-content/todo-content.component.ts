import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-todo-content',
  templateUrl: './todo-content.component.html',
  styleUrls: ['./todo-content.component.scss']
})
export class TodoContentComponent {
  @Input('todo') todo = ['application', 'submission', 'post'];
}
