import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailPageComponent } from './campaign-detail-page.component';

describe('CampaignDetailPageComponent', () => {
  let component: CampaignDetailPageComponent;
  let fixture: ComponentFixture<CampaignDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
