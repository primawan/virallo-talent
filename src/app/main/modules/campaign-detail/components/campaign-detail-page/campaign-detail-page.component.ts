import { Component, OnInit } from '@angular/core';
import { ResponsiveHelper } from '../../../../../common/helpers/responsive.helper';
import { Router } from '@angular/router';
import { DialogHelper } from '../../../../../common/helpers/dialog.helper';
import { TodoDialogComponent } from '../todo/todo-dialog/todo-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-campaign-detail-page',
  templateUrl: './campaign-detail-page.component.html',
  styleUrls: ['./campaign-detail-page.component.scss']
})
export class CampaignDetailPageComponent {
  responsiveMenu = new ResponsiveHelper();
  dialogHelper = new DialogHelper();
  dialogRef;

  constructor(private router: Router, public dialog: MatDialog) { }

  showingTodo(): boolean {
    let urlArr = ['/application', '/submission', '/post'];
    for (let url of urlArr) if (this.router.url.includes(url)) return true;
    return false;
  }

  openTodo() {
    this.dialogRef = this.dialog.open(TodoDialogComponent, this.dialogHelper.dialogOptions());
  }

  checkMenu(): boolean {
    return this.responsiveMenu.checkMenu(this.router, '/campaign/01', '/campaign/01/submission');
  }

}
