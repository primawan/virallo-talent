import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignLayoutComponent } from './campaign-layout.component';

describe('CampaignLayoutComponent', () => {
  let component: CampaignLayoutComponent;
  let fixture: ComponentFixture<CampaignLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
