import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailMenuComponent } from './campaign-detail-menu.component';

describe('CampaignDetailMenuComponent', () => {
  let component: CampaignDetailMenuComponent;
  let fixture: ComponentFixture<CampaignDetailMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDetailMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
