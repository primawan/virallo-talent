import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignDetailPageComponent } from './components/campaign-detail-page/campaign-detail-page.component';
import { SubmissionPageComponent } from './components/submission/submission-page/submission-page.component';
import { ApplicationPageComponent } from './components/application/application-page/application-page.component';

const routes: Routes = [
  { path: '', component: CampaignDetailPageComponent,
    children: [
      { path: 'brief' },
      { path: 'timeline' },
      { path: 'application', component: ApplicationPageComponent },
      { path: 'submission', component: SubmissionPageComponent },
      { path: 'post' }
    ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignDetailRoutingModule { }
