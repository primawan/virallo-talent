import { CommonCoreModule } from './../../../common/common.module';
import { NgModule } from '@angular/core';

import { CampaignDetailRoutingModule } from './campaign-detail-routing.module';
import { CampaignDetailPageComponent } from './components/campaign-detail-page/campaign-detail-page.component';
import { CampaignDetailMenuComponent } from './components/campaign-detail-menu/campaign-detail-menu.component';
import { CampaignDetailHeaderComponent } from './components/campaign-detail-header/campaign-detail-header.component';
import { SubmissionPageComponent } from './components/submission/submission-page/submission-page.component';
import { SubmissionFormComponent } from './components/submission/submission-form/submission-form.component';
import { SubmissionDisplayComponent } from './components/submission/submission-display/submission-display.component';
import { CampaignStatusComponent } from './components/campaign-status/campaign-status.component';
import { ApplicationPageComponent } from './components/application/application-page/application-page.component';
import { CampaignLayoutComponent } from './components/campaign-layout/campaign-layout.component';
import { TodoDialogComponent } from './components/todo/todo-dialog/todo-dialog.component';
import { TodoContentComponent } from './components/todo/todo-content/todo-content.component';

@NgModule({
  imports: [
    CommonCoreModule,
    CampaignDetailRoutingModule
  ],
  declarations: [
    CampaignDetailPageComponent,
    CampaignDetailMenuComponent,
    CampaignDetailHeaderComponent,
    SubmissionPageComponent,
    SubmissionFormComponent,
    SubmissionDisplayComponent,
    CampaignStatusComponent,
    ApplicationPageComponent,
    CampaignLayoutComponent,
    TodoDialogComponent,
    TodoContentComponent
  ],
  entryComponents: [
    TodoDialogComponent
  ]
})
export class CampaignDetailModule { }
