import { NgModule } from '@angular/core';
import { CommonCoreModule } from '../../../common/common.module';

import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationPageComponent } from './components/notification-page/notification-page.component';
import { NotificationItemComponent } from './components/notification-item/notification-item.component';

@NgModule({
  imports: [
    CommonCoreModule,
    NotificationRoutingModule
  ],
  declarations: [
    NotificationPageComponent,
    NotificationItemComponent
  ]
})
export class NotificationModule { }
