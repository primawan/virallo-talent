import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification-page',
  templateUrl: './notification-page.component.html',
  styleUrls: ['./notification-page.component.scss']
})
export class NotificationPageComponent implements OnInit {
  notifications = [1, 2, 3, 4, 5];

  constructor() { }

  ngOnInit() {
  }

}
