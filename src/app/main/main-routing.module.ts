import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './components/main-page/main-page.component';
import { AccountPageComponent } from './modules/account/components/account-page/account-page.component';

const routes: Routes = [
  { path: '', component: MainPageComponent,
    children: [
      { path: 'home', loadChildren: './modules/home/home.module#HomeModule' },
      { path: 'campaign', loadChildren: './modules/campaign/campaign.module#CampaignModule' },
      { path: 'analytic', loadChildren: './modules/analytic/analytic.module#AnalyticModule' },
      { path: 'notification', loadChildren: './modules/notification/notification.module#NotificationModule' },
      { path: 'account', loadChildren: './modules/account/account.module#AccountModule' },
      { path: '', redirectTo: '/account', pathMatch: 'full' }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
