import { CommonCoreModule } from './../common/common.module';
import { NgModule } from '@angular/core';

import { MainRoutingModule } from './main-routing.module';
import { MainPageComponent } from './components/main-page/main-page.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';

@NgModule({
  imports: [
    CommonCoreModule,
    MainRoutingModule
  ],
  declarations: [
    MainPageComponent,
    MainMenuComponent
  ]
})
export class MainModule { }
