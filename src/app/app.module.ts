import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonCoreModule } from './common/common.module';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import indonesian from '@angular/common/locales/id';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

registerLocaleData(indonesian);

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonCoreModule,

    // App Routing
    RouterModule.forRoot([
      { path: '', loadChildren: './main/main.module#MainModule' },
      { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
      { path: 'onboard', loadChildren: './onboard/onboard.module#OnboardModule' },
      { path: 'manager', loadChildren: './manager/manager.module#ManagerModule'},
      { path: '**', component: PageNotFoundComponent }
    ]),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'id' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
