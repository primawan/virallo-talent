import { NgModule } from '@angular/core';
import { CommonCoreModule } from '../common/common.module';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthPageComponent } from './components/auth-page/auth-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ForgotPassComponent } from './components/forgot-pass/forgot-pass.component';
import { ResetPassComponent } from './components/reset-pass/reset-pass.component';

@NgModule({
  imports: [
    CommonCoreModule,
    AuthRoutingModule
  ],
  declarations: [
    AuthPageComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPassComponent,
    ResetPassComponent
  ]
})
export class AuthModule { }
