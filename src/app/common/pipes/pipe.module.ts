import { NgModule } from '@angular/core';
import { NumberShortPipe } from './number/number-short.pipe';

@NgModule({
  declarations: [
    NumberShortPipe
  ],
  exports: [
    NumberShortPipe
  ]
})
export class PipeModule { }
