import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'numberShort'})
export class NumberShortPipe implements PipeTransform {

  transform(value: number, args?: any): string {
    let result = value.toString();
    let end = '';
    let divided = value / 1000;
    if (divided >= 1) {
      result = divided.toFixed(2);
      end = 'ribu';
    }
    if (divided >= 1000) {
      result = (divided / 1000).toFixed(2);
      end = 'juta';
    }
    if (divided >= 1000000) {
      result = (divided / 1000000).toFixed(2);
      end = 'miliar';
    }
    result = this.changeSeparator(result);
    return `${result}${end}`;
  }

  private changeSeparator(value: string): string {
    let result = value;
    let splitted = value.split('.');
    if (splitted.length > 1) {
      result = '';
      for (let split of splitted) if (split !== '00') result += `${split},`;
      result = result.slice(0, -1);
    }
    return result;
  }

}
