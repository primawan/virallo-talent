import { NgModule } from '@angular/core';
import { ResponsiveModule, ResponsiveConfig } from 'ng2-responsive';

export function ResponsiveDefinition() {
  let config = {
    breakPoints: {
      xs: { max: 575 }, sm: { min: 576, max: 767 },
      md: { min: 768, max: 991 }, lg: { min: 992, max: 1199 }, xl: { min: 1200 }
    },
    debounceTime: 50
  };
  return new ResponsiveConfig(config);
}

@NgModule({
  imports: [ResponsiveModule],
  exports: [ResponsiveModule],
  providers: [{ provide: ResponsiveConfig, useFactory: ResponsiveDefinition }]
})
export class ThirdResponsiveModule { }
