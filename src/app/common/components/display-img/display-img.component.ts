import { Component, Input } from '@angular/core';
import { AVATAR_URL } from '../../constants/ASSETS';

interface Options {
  not_round?: boolean;
  full?: boolean;
  small?: boolean;
  large?: boolean;
}

@Component({
  selector: 'app-display-img',
  templateUrl: './display-img.component.html',
  styleUrls: ['./display-img.component.scss']
})
export class DisplayImgComponent {
  @Input('image') image: string;
  @Input('options') options: Options = {} as any;

  avatarUrl = AVATAR_URL;
}
