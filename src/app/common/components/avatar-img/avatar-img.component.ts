import { Component, Input } from '@angular/core';

interface Options {
  showLogo?: boolean;
  small?: boolean;
  large?: boolean;
}

@Component({
  selector: 'app-avatar-img',
  templateUrl: './avatar-img.component.html',
  styleUrls: ['./avatar-img.component.scss']
})
export class AvatarImgComponent {
  @Input('options') options: Options = {} as any;
  @Input('logo') logo = 'instagram';
  @Input('avatarUrl') avatarUrl: string;
}
