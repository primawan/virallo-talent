import { Component, Input } from '@angular/core';

interface Options {
  small?: boolean;
  multiline?: boolean;
  textColor?: string;
}

@Component({
  selector: 'app-icon-text',
  templateUrl: './icon-text.component.html',
  styleUrls: ['./icon-text.component.scss']
})
export class IconTextComponent {
  @Input('icon') icon = 'info_outline';
  @Input('color') color = 'grey-second';
  @Input('options') options: Options = {} as any;

  getTextColor(): string {
    if (this.options.textColor === 'icon') return `text-${this.color}`;
    if (this.options.textColor) return `text-${this.options.textColor}`;
    return null;
  }
}
