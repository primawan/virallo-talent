import { Component, Input, Output, EventEmitter } from '@angular/core';

interface Options {
  noCloseBtn?: boolean;
}

@Component({
  selector: 'app-dialog-page',
  templateUrl: './dialog-page.component.html',
  styleUrls: ['./dialog-page.component.scss']
})
export class DialogPageComponent {
  @Input('header') header: string;
  @Input('options') options: Options = {} as any;
  @Output('actionClose') actionClose: EventEmitter<any> = new EventEmitter<any>();

  close() { this.actionClose.emit(); }
}
