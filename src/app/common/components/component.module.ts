import { ThirdResponsiveModule } from './../modules/third-responsive.module';
import { MaterialModule } from './../modules/material.module';
import { PipeModule } from './../pipes/pipe.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PageHeaderComponent } from './page-header/page-header.component';
import { InstagramSearchComponent } from './instagram-search/instagram-search.component';
import { SocialListComponent } from './social-list/social-list.component';
import { AvatarImgComponent } from './avatar-img/avatar-img.component';
import { IconTextComponent } from './icon-text/icon-text.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { DateLineComponent } from './date-line/date-line.component';
import { DisplayImgComponent } from './display-img/display-img.component';
import { TitleCollapseComponent } from './title-collapse/title-collapse.component';
import { LabelTextComponent } from './label-text/label-text.component';
import { DialogPageComponent } from './dialog-page/dialog-page.component';
import { CardItemComponent } from './card-item/card-item.component';
import { SideScrollComponent } from './side-scroll/side-scroll.component';
import { IconNumberComponent } from './icon-number/icon-number.component';
import { TabLabelComponent } from './tab-label/tab-label.component';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,
    MaterialModule,
    ThirdResponsiveModule
  ],
  declarations: [
    PageHeaderComponent,
    InstagramSearchComponent,
    SocialListComponent,
    AvatarImgComponent,
    IconTextComponent,
    MainHeaderComponent,
    DateLineComponent,
    DisplayImgComponent,
    TitleCollapseComponent,
    LabelTextComponent,
    DialogPageComponent,
    CardItemComponent,
    SideScrollComponent,
    IconNumberComponent,
    TabLabelComponent,
  ],
  exports: [
    CommonModule,
    PageHeaderComponent,
    InstagramSearchComponent,
    SocialListComponent,
    AvatarImgComponent,
    IconTextComponent,
    MainHeaderComponent,
    DateLineComponent,
    DisplayImgComponent,
    TitleCollapseComponent,
    LabelTextComponent,
    DialogPageComponent,
    CardItemComponent,
    SideScrollComponent,
    IconNumberComponent,
    TabLabelComponent,
  ]
})
export class ComponentModule { }
