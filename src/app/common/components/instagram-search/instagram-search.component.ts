import { AVATAR_URL } from './../../constants/ASSETS';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instagram-search',
  templateUrl: './instagram-search.component.html',
  styleUrls: ['./instagram-search.component.scss']
})
export class InstagramSearchComponent implements OnInit {
  selectedUsername;
  usernames = ['kancut', 'terbang', 'action'];
  avatarUrl = AVATAR_URL;

  constructor() { }

  ngOnInit() {
  }

  consoling(string) {
    console.log(string);
  }

}
