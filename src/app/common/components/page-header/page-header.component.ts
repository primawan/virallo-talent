import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

interface Options {
  noDivider?: boolean;
}

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {
  @Input('title') title: string;
  @Input('subtitle') subtitle: string;
  @Input('description') description: string;
  @Input('image') image: string;
  @Input('backTo') backTo: string;
  @Input('options') options: Options = {} as any;

  constructor(private router: Router, private location: Location) { }

  goBack() {
    if (this.backTo) this.router.navigate([this.backTo]);
    else this.location.back();
  }
}
