import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleCollapseComponent } from './title-collapse.component';

describe('TitleCollapseComponent', () => {
  let component: TitleCollapseComponent;
  let fixture: ComponentFixture<TitleCollapseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitleCollapseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
