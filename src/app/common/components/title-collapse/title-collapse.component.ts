import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title-collapse',
  templateUrl: './title-collapse.component.html',
  styleUrls: ['./title-collapse.component.scss']
})
export class TitleCollapseComponent {
  @Input('title') title: string;
  @Input('subtitle') subtitle: string;

  isOpen = false;
  expand() { this.isOpen = !this.isOpen; }
}
