import { Component, Input } from '@angular/core';

interface Options {
  imageTop: boolean;
}

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent {
  @Input('image') image: string;
  @Input('title') title: string;
  @Input('options') options: Options = {} as any;
}
