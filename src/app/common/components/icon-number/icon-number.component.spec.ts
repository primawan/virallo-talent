import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconNumberComponent } from './icon-number.component';

describe('IconNumberComponent', () => {
  let component: IconNumberComponent;
  let fixture: ComponentFixture<IconNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
