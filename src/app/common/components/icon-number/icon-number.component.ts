import { Component, Input } from '@angular/core';

interface Options {
  logoRight?: boolean;
  textColor?: string;
  short?: boolean;
}

@Component({
  selector: 'app-icon-number',
  templateUrl: './icon-number.component.html',
  styleUrls: ['./icon-number.component.scss']
})
export class IconNumberComponent {
  @Input('icon') icon = 'info_outline';
  @Input('number') number = 0;
  @Input('color') color = 'grey-second';
  @Input('options') options: Options = {} as any;

  getSideClass() {
    return {
      'pl-1': !this.options.logoRight,
      'pr-1': this.options.logoRight,
      'order-first': this.options.logoRight,
    };
  }

  getTextClass(): string {
    let textClass = 'large';
    if (this.options.textColor === 'icon') return textClass += ` text-${this.color}`;
    if (this.options.textColor) return textClass += ` text-${this.options.textColor}`;
    return textClass;
  }
}
