import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AVATAR_URL } from '../../constants/ASSETS';

@Component({
  selector: 'app-social-list',
  templateUrl: './social-list.component.html',
  styleUrls: ['./social-list.component.scss']
})
export class SocialListComponent implements OnInit {
  @Output('socialItem') socialItem = new EventEmitter();
  avatarUrl = AVATAR_URL;

  constructor() { }

  ngOnInit() {
  }

  selectSocial(social: string) {
    // if () there is no social
    switch (social) {
      case 'instagram':
        this.socialItem.emit('instagram');
        break;
      case 'youtube':
        // disabled for now
        break;
    }
  }

}
