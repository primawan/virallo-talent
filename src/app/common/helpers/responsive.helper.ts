import { Router } from '@angular/router';

const BREAKPOINT = 768;

export class ResponsiveHelper {

  checkMobile() {
    if (window.innerWidth < BREAKPOINT) return true;
    return false;
  }

  checkMenu(router: Router, url: string, urlDefault: string): boolean {
    if (this.checkMobile()) {
      if (router.url === url) return true;
      return false;
    } else {
      if (router.url === url) {
        router.navigate([urlDefault]);
        return false;
      }
      return true;
    }
  }
}
