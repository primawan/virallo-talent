import { DialogPageComponent } from '../components/dialog-page/dialog-page.component';
import { ResponsiveHelper } from './responsive.helper';

export class DialogHelper {
  private responsiveHelper = new ResponsiveHelper();
  constructor () {}

  dialogOptions(data?: any) {
    return {
      data: data || null,
      maxWidth: this.dialogSize,
      maxHeight: this.dialogSize,
      panelClass: 'responsive-dialog',
      autoFocus: false,
    };
  }

  get dialogSize(): string {
    if (this.responsiveHelper.checkMobile()) return '100%';
    return '85%';
  }
}
