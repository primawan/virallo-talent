import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentModule } from './components/component.module';
import { PipeModule } from './pipes/pipe.module';
import { ThirdResponsiveModule } from './modules/third-responsive.module';

@NgModule({
  imports: [
    ComponentModule,
    PipeModule,
    MaterialModule,
    ThirdResponsiveModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    ComponentModule,
    PipeModule,
    MaterialModule,
    ThirdResponsiveModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class CommonCoreModule { }
